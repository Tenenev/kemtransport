﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.ViewModels.Shared;
using HtmlAgilityPack;

namespace Client.Common
{
    public class ClientResultHelper : ResultHelper
    {
        public override async Task<ResultViewModel> GetResult(string resultStr)
        {
            var task = new Task<ResultViewModel>(() =>
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(resultStr);

                var model = new ResultViewModel();

                try
                {
                    var tables =
                        doc.DocumentNode.Descendants("table")
                            .Where(
                                t =>
                                    t.Attributes.Contains("style") &&
                                    t.Attributes["style"].Value == "page-break-inside: avoid;")
                            .ToList();

                    var dtToday = DateTime.Today;
                    foreach (var table in tables)
                    {
                        //cutthis
                        var container = table.Descendants("div")
                            .FirstOrDefault(
                                d => d.Attributes.Contains("class") && d.Attributes["class"].Value == "cutthis");

                        if(container == null) continue;

                        var currentStop = new StopScheduleViewModel { Name = System.Net.WebUtility.HtmlDecode(container.Element("p").InnerText) };

                        var schesuleTable = container.Element("table");

                        if(schesuleTable == null) continue;

                        // Parse table
                        foreach (var scheduleTr in schesuleTable.Elements("tr"))
                        {
                            var dtNodes = scheduleTr.Elements("td").ToList();

                            for (var i = 0; i < dtNodes.Count; i += 2)
                            {
                                var hourTd = dtNodes[i];
                                var minutesTd = dtNodes[i + 1];

                                var hourStr = hourTd.Element("span").InnerText;

                                var minutesStr =
                                    minutesTd.Elements("span")
                                        .Select(s => s.InnerText)
                                        .Where(t => !string.IsNullOrWhiteSpace(t))
                                        .ToList();

                                var minutes = (from minuteStr in minutesStr
                                    select new string(minuteStr.TakeWhile(Char.IsDigit).ToArray())
                                    into clearMinuteStr
                                    where clearMinuteStr.Length != 0
                                    select int.Parse(clearMinuteStr)).ToList();

                                if (string.IsNullOrWhiteSpace(hourStr) || minutes.Count == 0) continue;

                                var hour = int.Parse(hourStr);


                                foreach (
                                    var time in
                                        minutes.Select(
                                            m => new DateTime(dtToday.Year, dtToday.Month, dtToday.Day, hour, m, 0)))
                                    currentStop.TimeList.Add(time);
                            }
                        }
                        currentStop.Calculate();
                        model.Schedule.Add(currentStop);
                    }

                    return model;
                }
                catch (Exception exc)
                {
                    return null;
                }
            });

            task.Start();
            return await task;
        }
    }
}
