﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.Storage.Streams;
using Windows.Web.Http;
using Client.ViewModels.Shared;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Client.Common;
using HtmlAgilityPack;
using Encoding = Portable.Text.Encoding;

namespace Client.Common
{
    public static class ApiHelper
    {
        private static List<RouteViewModel> Routes = new List<RouteViewModel>();

        public const string ApiBaseUrl = @"http://kemgortrans.ru/navi/schedule/";

        private static async Task<string> GetResponse(string parameters)
        {
            string response;
            var utf8 = Encoding.UTF8;
            var win1251 = Encoding.GetEncoding("Windows-1251");

            using (var client = new HttpClient())
            {
                var request = ApiBaseUrl + Encode(parameters);

                var stream = await client.GetInputStreamAsync(new Uri(request));

                byte[] bytes;

                using (var ms = new MemoryStream())
                {
                    stream.AsStreamForRead().CopyTo(ms);
                    bytes = ms.ToArray();
                }
                
                var win1251Bytes = Encoding.Convert(win1251, Encoding.UTF8, bytes);

                response = utf8.GetString(win1251Bytes, 0, win1251Bytes.Length);
            }

            return response;
        }

        private static string Encode(string str)
        {
            var result = new StringBuilder();

            const string letters = @"АБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзиклмнопрстуфхцчшщъыьэюя";
            var encoding = Encoding.GetEncoding("windows-1251");

            foreach (var ch in str)
            {
                if (letters.Contains(ch))
                {
                    var bytes = encoding.GetBytes(new[] { ch });
                    var hex = BitConverter.ToString(bytes);
                    result.Append("%" + hex.Replace("-", "%"));
                }
                else
                {
                    result.Append(ch);
                }
            }

            return result.ToString();
        }

        public static async Task LoadRoutes()
        {
            await LoadRoutesByType("avto");
            await LoadRoutesByType("trol");
            await LoadRoutesByType("tram");
        }

        private static async Task LoadRoutesByType(string routeType)
        {
            var response = await GetResponse(string.Format("simplified.php?type={0}", routeType));
            var doc = new HtmlDocument();
            doc.LoadHtml(response);

            var routeNodes = doc.DocumentNode.Descendants("a")
                .Where(
                    n => n.Attributes.Contains("href") && n.Attributes["href"].Value.StartsWith("simplified.php?type="))
                .ToList();

            foreach (var routeNode in routeNodes)
            {
                var routeModel = new RouteViewModel
                {
                    DisplayNum = routeNode.InnerText,
                    Type = routeType
                };
                var hrefAttribute = routeNode.Attributes["href"];
                if (hrefAttribute == null) continue;
                routeModel.Url = hrefAttribute.Value;

                var orderNumStr = new string(routeNode.InnerText.Where(Char.IsDigit).ToArray());

                if (orderNumStr.Length != 0)
                    routeModel.OrderNum = int.Parse(orderNumStr);

                Routes.Add(routeModel);
            }
        }

        public static async Task<List<RouteViewModel>> GetRoutes(string searchStr)
        {
            return Routes.Where(r => r.DisplayNum.Contains(searchStr)).OrderBy(r => r.Type).ThenBy(r => r.OrderNum).ToList();
        }

        public static async Task<List<ScheduleViewModel>> GetSchedules(string url)
        {
            var response = await GetResponse(url);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);

            var scheduleNodes = doc.DocumentNode.Descendants("a")
                .Where(
                    n => n.Attributes.Contains("href") && n.Attributes["href"].Value.StartsWith("simplified.php?type="))
                .ToList();

            var schedules = new List<ScheduleViewModel>();
            foreach (var scheduleNode in scheduleNodes)
            {
                var model = new ScheduleViewModel();

                var hrefAttribute = scheduleNode.Attributes["href"];
                if (hrefAttribute == null) continue;
                model.Url = hrefAttribute.Value;

                model.Title = scheduleNode.InnerText;

                schedules.Add(model);
            }
            return schedules.OrderBy(s => s.Title).ToList();
        }

        public static async Task<List<DirectionViewModel>> GetDirections(string url)
        {
            var response = await GetResponse(url);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);

            var scheduleNodes = doc.DocumentNode.Descendants("a")
                .Where(
                    n => n.Attributes.Contains("href") && n.Attributes["href"].Value.StartsWith("simplified.php?type="))
                .ToList();

            var directions = new List<DirectionViewModel>();
            foreach (var scheduleNode in scheduleNodes)
            {
                var model = new DirectionViewModel();

                var hrefAttribute = scheduleNode.Attributes["href"];
                if (hrefAttribute == null) continue;
                model.Url = hrefAttribute.Value;
                model.Title = scheduleNode.InnerText;
                directions.Add(model);
            }
            return directions.OrderBy(s => s.Title).ToList();
        }

        public static async Task<List<StopViewModel>> GetStops(string url)
        {
            var response = await GetResponse(url);

            var doc = new HtmlDocument();
            doc.LoadHtml(response);

            var scheduleNodes = doc.DocumentNode.Descendants("a")
                .Where(
                    n => n.Attributes.Contains("href") && n.Attributes["href"].Value.StartsWith("shedule.printable.php?type="))
                .ToList();

            var stops = new List<StopViewModel>();
            foreach (var scheduleNode in scheduleNodes)
            {
                var model = new StopViewModel();

                var hrefAttribute = scheduleNode.Attributes["href"];
                if (hrefAttribute == null) continue;
                model.Url = hrefAttribute.Value;
                model.Title = scheduleNode.InnerText;
                stops.Add(model);
            }
            return stops.ToList();
        }

        public static async Task<string> GetResult(string url)
        {
            return await GetResponse(url);
        }
    }
}
