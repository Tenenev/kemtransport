﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.ViewModels.Shared
{
    public class ScheduleViewModel
    {
        public string Url { get; set; }

        public string Title { get; set; }
    }
}
