﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Client.ViewModels.Shared
{
    public class ResultViewModel
    {
        public string Title { get; set; }

        public string Date { get; set; }

        public ObservableCollection<StopScheduleViewModel> Schedule { get; set; }

        public ResultViewModel()
        {
            Schedule = new ObservableCollection<StopScheduleViewModel>();
        }
    }
}