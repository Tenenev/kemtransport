﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.ViewModels.Shared
{
    public class HourScheduleViewModel
    {
        public string Hour { get; set; }

        public List<string> Minutes { get; set; }

        public HourScheduleViewModel()
        {
            Minutes = new List<string>();
        }
    }
}
