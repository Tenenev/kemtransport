﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.ViewModels.Shared
{
    public class RouteViewModel
    {
        public string Url { get; set; }

        public string Type { get; set; }

        public string DisplayNum { get; set; }

        public int OrderNum { get; set; }

        public string FullDisplayNum
        {
            get
            {
                var result = new StringBuilder(DisplayNum);
                switch (Type)
                {
                    case "avto":
                        result.Append(" автобус");
                        break;
                    case "trol":
                        result.Append(" троллейбус");
                        break;
                    case "tram":
                        result.Append(" трамвай");
                        break;
                }
                return result.ToString();
            }
        }
    }
}
